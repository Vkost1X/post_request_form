from flask import Flask, render_template, request, redirect, session, url_for
import json

app = Flask(__name__, template_folder= 'templates')
app.secret_key = "created_by_voronovich"

@app.route("/")
def home():
    print(session)
    return render_template("index.html")

@app.route("/feedback", methods = ["POST", "GET"])
def feedback():
    if request.method == "POST":

        def load_data(data):
            '''
            def get_id(data): return len(data)

            json_data = json.load(open("data.json",))
            data_dict = {}
            for key, value in request.form:
                data_dict[key] = value
            json_data[get_id(json_data)] = data_dict
            json.dump(json_data, open("data.json", "w"))
        '''
        print(request.form)
        session["user"] = request.form['name']
        #load_data(request.form)
        return redirect(url_for("success"))
    else: return render_template("feedback.html")


@app.route("/success")
def success():
    if "user" in session: return render_template("success.html")
    else: return redirect(url_for("home"))

if __name__ == "__main__":
    app.run(debug = True)